import matplotlib.pyplot as plt
import pandas as pd

t = input("Enter the name of the csv file with its path - ")
df = pd.read_csv(t, header=None, names=['a', 'time','Deg_C','Bet_C','Close_C','Eig_C','Global CC','Assor','Dia', 'avg_deg','modularity','density','avg_path_length','ver','ed'])
list5 = df['Global CC'].tolist()[1:]
list1 = df['avg_deg'].tolist()[1:]
list2 = df['modularity'].tolist()[1:]
list3 = df['density'].tolist()[1:]
list4 = df['avg_path_length'].tolist()[1:]
list6 = df['Deg_C'].tolist()[1:]
list7 = df['Bet_C'].tolist()[1:]
list8 = df['Close_C'].tolist()[1:]
list9 = df['Eig_C'].tolist()[1:]
list10 = df['Assor'].tolist()[1:]
list11 = df['Dia'].tolist()[1:]
list12 = df['ver'].tolist()[1:]
list13 = df['ed'].tolist()[1:]
	
list1 = [float(i) for i in list1] 
list2 = [float(i) for i in list2] 
list3 = [float(i) for i in list3] 
list4 = [float(i) for i in list4] 
list5 = [float(i) for i in list5] 
list6 = [float(i) for i in list6] 
list7 = [float(i) for i in list7] 
list8 = [float(i) for i in list8] 
list9 = [float(i) for i in list9] 
list10 = [float(i) for i in list10] 
list11 = [float(i) for i in list11] 
list12 = [float(i) for i in list12] 
list13 = [float(i) for i in list13] 

#print(list1)
#print(list2)
#print(list3)
#print(list4)

time = [i for i in range(40)]


x = input("Enter value for p_oo - ")
y = input("Enter value for p_on - ")
z = input("Enter value for p_nn - ")
'''
N = "p_oo =" + x + " p_on = " + y + " p_nn = " + z
plt.title(N)

plt.xlabel('Timestamp')
plt.ylabel(' Network Parameters ')

#plt.plot(time, list1, 'b-',label='Average Degree', marker='o', markerfacecolor='black', markersize=3)
#for a,b in zip(time, list1): 
#	plt.text(a, b, " ")

plt.plot(time, list2, 'g-',label='Modularity',marker='o', markerfacecolor='black', markersize=3)
for a,b in zip(time, list2): 
	plt.text(a, b, " " ,fontsize=7)

plt.plot(time, list3, 'y-',label='Density',marker='o', markerfacecolor='black', markersize=3)
for a,b in zip(time, list3): 
	plt.text(a, b, " ",fontsize=7)

plt.plot(time, list4, 'r-',label='Average Path Length',marker='o', markerfacecolor='black', markersize=3)
for a,b in zip(time, list4): 
	plt.text(a, b, " ",fontsize=7)

plt.legend()
'''

plt.style.use('ggplot') 
  
# defining subplots and their positions 
#plt1 = plt.subplot2grid((11,1), (0,0), rowspan = 3, colspan = 1) 
#plt2 = plt.subplot2grid((11,1), (4,0), rowspan = 3, colspan = 1) 
#plt3 = plt.subplot2grid((11,1), (8,0), rowspan = 3, colspan = 1) 
plt4 = plt.subplot2grid((11,1), (0,0), rowspan = 3, colspan = 1) 
plt5 = plt.subplot2grid((11,1), (4,0), rowspan = 3, colspan = 1) 
plt6 = plt.subplot2grid((11,1), (8,0), rowspan = 3, colspan = 1) 
#plt7 = plt.subplot2grid((11,1), (0,0), rowspan = 3, colspan = 1) 
#plt8 = plt.subplot2grid((11,1), (4,0), rowspan = 3, colspan = 1) 
#plt9 = plt.subplot2grid((11,1), (8,0), rowspan = 3, colspan = 1) 
N = "p_oo = " + x + ", p_on = " + y + ", p_nn = " + z

'''
plt1.set_title(N)

plt2.set_ylabel(' Network Parameters ')

plt3.set_xlabel('Timestamp')
'''

plt4.set_title(N)

plt5.set_ylabel(' Network Parameters ')

plt6.set_xlabel('Timestamp')
'''
plt7.set_title(N)

plt8.set_ylabel(' Network Parameters ')

plt9.set_xlabel('Timestamp')
'''

# plotting points on each subplot 
'''
plt1.plot(time, list1, 'b-',label='Average Degree', marker='o', markerfacecolor='black', markersize=3)
plt2.plot(time, list3, 'y-',label='Density',marker='o', markerfacecolor='black', markersize=3)

plt2.plot(time, list6, 'k-',label='Degree Centrality',marker='o', markerfacecolor='black', markersize=3)

plt2.plot(time, list8, 'b-',label='Closeness Centrality',marker='o', markerfacecolor='black', markersize=3)

plt2.plot(time, list5, 'c-',label='Global Clustering Coefficient',marker='o', markerfacecolor='black', markersize=3)

plt3.plot(time, list4, 'r-',label='Average Path Length',marker='o', markerfacecolor='black', markersize=3)
  
plt2.plot(time, list2, 'g-',label='Modularity',marker='o', markerfacecolor='black', markersize=3)

'''
plt4.plot(time, list7, 'b-',label='Betweenness Centrality',marker='o', markerfacecolor='black', markersize=3)

plt5.plot(time, list9, 'm-',label='Eigenvector Centrality',marker='o', markerfacecolor='black', markersize=3)

plt6.plot(time, list10, 'g-',label='Assortativity',marker='o', markerfacecolor='black', markersize=3)

'''
plt7.plot(time, list11, 'y-',label='Diameter',marker='o', markerfacecolor='black', markersize=3)

plt8.plot(time, list12, 'b-',label='Vertices',marker='o', markerfacecolor='black', markersize=3)

plt9.plot(time, list13, 'r-',label='Edges',marker='o', markerfacecolor='black', markersize=3)


# show legends of each subplot 
'''

'''
plt1.legend() 
plt2.legend() 
plt3.legend() 
'''

plt4.legend()
plt5.legend() 
plt6.legend() 
'''
plt7.legend() 
plt8.legend() 
plt9.legend() 
'''


# function to show plot 
plt.show()
