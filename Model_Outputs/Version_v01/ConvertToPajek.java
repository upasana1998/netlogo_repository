import java.io.*;

public class ConvertToPajek {
	public static void main(String args[]) throws IOException{
		//String inFolder=args[0];
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter input folder address:");
		String inFolder = br1.readLine();
		System.out.println("Enter output folder address:");
		String outFolder = br1.readLine();
		File file = new File(inFolder);
		File[] files = file.listFiles();
		//for(File f:files){
		//	System.out.println(f.getName());
		//}
		for(File f: files){
			String current = f.getName();
			//System.out.println(current);
			BufferedReader br = new BufferedReader(new FileReader(inFolder+"/"+current));
			String line = br.readLine();
			File theDir = new File(outFolder+"/"+current);
			/*if (!theDir.exists()) {
				try{
					theDir.mkdirs();
					//result = true;
		    		} 
		    		catch(SecurityException se){
					se.printStackTrace();
		    		}
			}*/
			theDir.createNewFile();
			FileWriter f2 = new FileWriter(theDir);
			while(line!=null){
				if(line.contains("Vertices"))
					f2.write("*"+line+System.lineSeparator());
				else if(line.contains("Edges"))
					f2.write("*"+line+System.lineSeparator());
				else
					f2.write(line+System.lineSeparator());
				//oldContent = oldContent + line + System.lineSeparator();
				line = br.readLine();
			}
			//String newContent = oldContent.replaceAll("#","");
			//newContent = newContent.replaceAll("Vertices","*Vertices");
			//newContent = newContent.replaceAll("Edge","*Edge");
			//FileWriter fw = new FileWriter(inFolder+"/"+current);
			//fw.write(newContent);
			br.close();
			f2.close();
		}
	}
}
