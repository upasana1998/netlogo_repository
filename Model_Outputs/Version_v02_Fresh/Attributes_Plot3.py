import pandas as pd
import matplotlib.pyplot as plt

t = input("Enter the name of the csv file with its path - ")
df = pd.read_csv(t, header=None, names=['a', '136','163','316','361','613','631'])
list1 = df['136'].tolist()[1:]
list2 = df['163'].tolist()[1:]
list3 = df['316'].tolist()[1:]
list4 = df['361'].tolist()[1:]
list5 = df['613'].tolist()[1:]
list6 = df['631'].tolist()[1:]
	
list1 = [float(i) for i in list1] 
list2 = [float(i) for i in list2] 
list3 = [float(i) for i in list3] 
list4 = [float(i) for i in list4] 
list5 = [float(i) for i in list5] 

#print(list1)
#print(list2)
#print(list3)
#print(list4)

time = [i for i in range(40)]


x = input("Enter the attribute name - ")

N = "Plots for " + x


plt.style.use('ggplot') 
  
# defining subplots and their positions 
plt1 = plt.subplot2grid((11,1), (0,0), rowspan = 10, colspan = 1) 


plt1.set_title("Plots with different values of p_oo, p_on and p_nn")

plt1.set_ylabel(' Value ')

plt1.set_xlabel('Timestamp')

# plotting points on each subplot 

plt1.plot(time, list1, 'b-',label= "p_oo = 0.1, p_on = 0.3, p_nn = 0.6", marker='o', markerfacecolor='black', markersize=3)

plt1.plot(time, list2, 'g-',label= "p_oo = 0.1, p_on = 0.6, p_nn = 0.3", marker='o', markerfacecolor='black', markersize=3)

plt1.plot(time, list3, 'r-',label= "p_oo = 0.3, p_on = 0.1, p_nn = 0.6", marker='o', markerfacecolor='black', markersize=3)

plt1.plot(time, list4, 'y-',label= "p_oo = 0.3, p_on = 0.6, p_nn = 0.1", marker='o', markerfacecolor='black', markersize=3)

plt1.plot(time, list5, 'm-',label= "p_oo = 0.6, p_on = 0.1, p_nn = 0.3", marker='o', markerfacecolor='black', markersize=3)

plt1.plot(time, list6, 'c-',label= "p_oo = 0.6, p_on = 0.3, p_nn = 0.1", marker='o', markerfacecolor='black', markersize=3)


plt1.legend() 

# function to show plot 
plt.show()
