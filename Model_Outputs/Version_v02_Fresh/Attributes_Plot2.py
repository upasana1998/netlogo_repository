import pandas as pd
import matplotlib.pyplot as plt

t = input("Enter the name of the csv file with its path - ")
df = pd.read_csv(t, header=None, names=['a', '136','163','316','361','613','631'])
list1 = df['136'].tolist()[1:]
list2 = df['163'].tolist()[1:]
list3 = df['316'].tolist()[1:]
list4 = df['361'].tolist()[1:]
list5 = df['613'].tolist()[1:]
list6 = df['631'].tolist()[1:]

print(list5, list6)
	
list1 = [float(i) for i in list1] 
list2 = [float(i) for i in list2] 
list3 = [float(i) for i in list3] 
list4 = [float(i) for i in list4] 
list5 = [float(i) for i in list5] 
list6 = [float(i) for i in list6] 

#print(list1)
#print(list2)
#print(list3)
#print(list4)

time = [i for i in range(40)]


x = input("Enter the attribute name - ")


plt.style.use('ggplot') 
  
# defining subplots and their positions 
plt1 = plt.subplot2grid((11,1), (0,0), rowspan = 3, colspan = 1) 
plt2 = plt.subplot2grid((11,1), (4,0), rowspan = 3, colspan = 1) 
plt3 = plt.subplot2grid((11,1), (8,0), rowspan = 3, colspan = 1) 


plt1.set_title("p_oo = 0.3, p_on = 0.6, p_nn = 0.1")
plt2.set_title("p_oo = 0.6, p_on = 0.1, p_nn = 0.3")
plt3.set_title("p_oo = 0.6, p_on = 0.3, p_nn = 0.1")

plt1.set_ylabel(' Value ')

plt2.set_ylabel(' Value ')

plt3.set_ylabel(' Value ')

plt3.set_xlabel('Timestamp')
# plotting points on each subplot 

plt1.plot(time, list4, 'b-',label=x, marker='o', markerfacecolor='black', markersize=3)

plt2.plot(time, list5, 'g-',label=x, marker='o', markerfacecolor='black', markersize=3)

plt3.plot(time, list6, 'r-',label=x, marker='o', markerfacecolor='black', markersize=3)


plt1.legend() 
plt2.legend() 
plt3.legend() 


# function to show plot 
plt.show()
